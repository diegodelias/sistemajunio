<?php
require_once "conexion.php";


/**crear categoria */

class ModeloCategorias{

static public function mdlIngresarCategoria($tabla,$datos){

    $conexionz = new Conexion();

    $stmt = $conexionz->conectar()->prepare("INSERT INTO $tabla(categoria) VALUES(:categoria)");

    $stmt -> bindParam(":categoria",$datos, PDO::PARAM_STR);

    if($stmt->execute()){
        return "ok";


    }else {

        return "error";

    }

    
		$stmt->close();
		$stmt = null;


}


static public function mdlMostrarCategorias($tabla,$item,$valor){
    if($item != null){
        $conexionz = new Conexion();
        $stmt = $conexionz->conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");
        $stmt -> bindParam(":".$item,$valor, PDO::PARAM_STR);
        $stmt -> execute();
        return $stmt -> fetch();

    }else{
        $conexionz = new Conexion();

        $stmt = $conexionz->conectar()->prepare("SELECT * FROM $tabla");
        $stmt -> execute();
        return $stmt -> fetchAll();

    }

    $stmt -> close();
    $stmt = null;

}

static public function mdlEditarCategoria($tabla,$datos){

    $conexionz = new Conexion();

    $stmt = $conexionz->conectar()->prepare("UPDATE $tabla SET categoria = :categoria WHERE id =:id");

    $stmt -> bindParam(":categoria",$datos["categoria"], PDO::PARAM_STR);
    $stmt -> bindParam(":id",$datos["id"], PDO::PARAM_INT);

    if($stmt->execute()){
        return "ok";


    }else {

        return "error";

    }

    
		$stmt->close();
		$stmt = null;


}

    
// borrar categoria
static public function mdlBorrarCategoria($tabla,$datos){

    $conexionz = new Conexion();

	$stmt = $conexionz->conectar()->prepare("DELETE FROM $tabla WHERE id = :id");
	$stmt -> bindParam(":id",$datos, PDO::PARAM_INT); /**BINDEA parmetro  datos con
    "id" en la consulta */
    

    if($stmt->execute()){
        return "ok";


    }else {

        return "error";

    }

    
		$stmt->close();
		$stmt = null;

}

}

?>