<?php


class ControladorCategorias{
        /***metodo crear categorias */

        static public function ctrCrearCategoria(){

            if(isset($_POST["nuevaCategoria"])){
                
            if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["nuevaCategoria"])){/** permite de a a z minuscula y mayuscula numero del 0 al nueve y caracteres latinos en minuscula y mayuscula y espacio en blanco */
                

                $tabla = "categorias";
                $datos = $_POST["nuevaCategoria"];
                $respuesta = ModeloCategorias::mdlIngresarCategoria($tabla,$datos);

                if($respuesta == "ok"){

                    echo '<script>

                    swal({
                       
                        
                        text: "El categoria ha sido guardada correctamente ",
                        icon: "success",
                        buttons: true,
                        dangerMode: false,
                    
                    }).then((result)=>{
                        if(result){
    
                                window.location = "categorias";
                        }
    
    
                        }) ;
    
        
                    
                    
    
                    </script>';
    




                }



            }else{

                
                echo '<script>

                swal({
                   
                    
                    text: "La categoría  no puede ir vacía o llevar caracteres especiales!",
                    icon: "error",
                    buttons: true,
                    dangerMode: true,
                
                }).then((result)=>{
                    if(result){

                            window.location = "categorias";
                    }


                    }) ;

                </script>';

            }
        }

        }

        static public function ctrMostrarCategorias($item,$valor){
            $tabla = "categorias";
            
            $respuesta = ModeloCategorias::mdlMostrarCategorias($tabla,$item,$valor);
            return $respuesta;

        }


/**Editar categoria */
        
        static public function ctrEditarCategoria(){

            if(isset($_POST["editarCategoria"])){
                
            if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["editarCategoria"])){/** permite de a a z minuscula y mayuscula numero del 0 al nueve y caracteres latinos en minuscula y mayuscula y espacio en blanco */
                

                $tabla = "categorias";
                $datos = array("categoria" => $_POST["editarCategoria"],
                 "id"=>$_POST["idCategoria"] );
                $respuesta = ModeloCategorias::mdlEditarCategoria($tabla,$datos);

                if($respuesta == "ok"){

                    echo '<script>

                    swal({
                       
                        
                        text: "El categoria ha sido cambiada correctamente ",
                        icon: "success",
                        buttons: true,
                        dangerMode: false,
                    
                    }).then((result)=>{
                        if(result){
    
                                window.location = "categorias";
                        }
    
    
                        }) ;
    
        
                    
                    
    
                    </script>';
    




                }



            }else{

                
                echo '<script>

                swal({
                   
                    
                    text: "La categoría  no puede ir vacía o llevar caracteres especiales!",
                    icon: "error",
                    buttons: true,
                    dangerMode: true,
                
                }).then((result)=>{
                    if(result){

                            window.location = "categorias";
                    }


                    }) ;

                </script>';

            }
        }

        }




        static function ctrBorrarCategoria(){

            if(isset($_GET["idCategoria"])){
                $tabla="categorias";
                $datos=$_GET["idCategoria"];
           
                $respuesta = ModeloCategorias::mdlBorrarCategoria($tabla,$datos);
        
                if($respuesta =="ok"){
        
        
                    echo '<script>
        
                    swal({
                       
                        
                        text: "La categoria ha sido borrado correctamente",
                        icon: "success",
                        buttons: true,
                        dangerMode: true,
                    
                    }).then((result)=>{
                        if(result){
        
                                window.location = "categorias"; 
                        }
        
        
                        }) ;
        
        
                    
                    
        
                    </script>';
                  
        
        
        
                }
        
        
        
            }
        }
        


}





?>