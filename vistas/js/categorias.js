$(".btnEditarCategoria").click(function(){

    var idCategoria = $(this).attr("idCategoria");
     /**guardar el id de la fila de la tabla  traido de la base de datos */
  
    var datos = new FormData();
    datos.append("idCategoria", idCategoria);
    
    $.ajax({
        url:"ajax/categorias.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType:"json",
        success: function(respuesta){

            $("#editarCategoria").val(respuesta["categoria"]);
            $("#idCategoria").val(respuesta["id"]);/**carga id de categoria en input oculto en la vista */


          
          
        }

    })




})


/**Eliminar */

$(".btnEliminarCategoria").click(function(){

    var  IdCategoria = $(this).attr("idCategoria");

    swal({
        title: 'Esta seguro de borrar la categoria',
        text: "¡Si no lo está puede cancelar la acción",
        icon: 'warning',
        // showCancelButton: true,
        buttons:true,
        dangerMode:true,
        buttons: ["Cancelar", "Borrar"],
        // confirmButtonColor: '#3085d',
        // cancelButtonColor: "#d33",
        cancelButtonText: 'Cancelar',
        // confirmButtonText: 'Si, borrar usuario!'

}).then((result)=>{
        /**si se selecciono boton eliminar redirecciona con variable get con id de usuario */
        if(result){
                window.location = "index.php?ruta=categorias&idCategoria="+IdCategoria;

        }

})


})